main = do
    putStrLn "Start at positive integer: "
    startAt <- getLine
    putStrLn "How many primes to get: "
    numPrimes <- getLine
    let rStartAt = read startAt :: Int
    let rNumPrimes = read numPrimes :: Int
    let arr = getNextPrimes rStartAt rNumPrimes
    putStrLn ("Primes: " ++ show arr)

getNextPrimes :: Int -> Int -> [Int]
getNextPrimes _ 0 = []
getNextPrimes x y
    | isPrime x = [x] ++ getNextPrimes (x+1) (y-1)
    | otherwise = getNextPrimes (x+1) y

isPrime :: Int -> Bool
isPrime 1 = False
isPrime n = not (0 `elem` primeList)
            where primeList = map (mod n)  [2..n `div` 2]
