main = do
    putStrLn "This program will generate the nth Fibonacci number"
    putStrLn "Enter n: "
    n <- getLine
    let num = read n :: Int
    putStrLn ("The number was " ++ show (fibonacci num))
    putStrLn ""
    main

fibonacci :: Int -> Int
fibonacci n
    | n <= 0    = 0
    | n < 3     = 1
    | otherwise = fibonacci (n-1) + fibonacci(n-2)