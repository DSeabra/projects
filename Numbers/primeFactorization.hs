main = do
    putStrLn "Prime factorization of: "
    str <- getLine
    putStrLn "I'm going to go out on a limb and hope you gave me an integer."
    putStrLn "If you didn't, screw you."
    putStrLn ("Factorization is: " ++ show (primeFactorization (read str)))
    putStrLn ""
    main

primeFactorization :: Integer -> [Integer]
primeFactorization 1 = [1]
primeFactorization n
    | n < 1     = []
    | isPrime n = [n]
    | otherwise = [m] ++ primeFactorization (n `div` m)
        where m = head (filter (isDivisible n) [2..n `div` 2])

isPrime :: Integer -> Bool
isPrime 1 = False
isPrime n = not (0 `elem` primeList)
            where primeList = map (mod n)  [2..n `div` 2]

isDivisible :: Integer -> Integer -> Bool
isDivisible _ 0 = False
isDivisible m n = m `mod` n == 0
