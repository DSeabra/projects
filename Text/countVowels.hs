-- Count Vowels – Enter a string and the program counts the number of vowels
-- in the text. For added complexity have it report a sum of each vowel found.
import Data.Char  -- To use function 'toLower' in chars

main = do
    putStrLn "Please enter the string to be analyzed: "
    str <- getLine
    putStrLn ("You entered: " ++ str)
    let num_vowels = countLetter 'a' str + countLetter 'e' str +
                     countLetter 'i' str + countLetter 'o' str +
                     countLetter 'u' str
    putStrLn ("Total vowels: " ++ show num_vowels)
    putStrLn ("Occurrences of the letter 'A:' " ++ show (countLetter 'a' str))
    putStrLn ("Occurrences of the letter 'E:' " ++ show (countLetter 'e' str))
    putStrLn ("Occurrences of the letter 'I:' " ++ show (countLetter 'i' str))
    putStrLn ("Occurrences of the letter 'O:' " ++ show (countLetter 'o' str))
    putStrLn ("Occurrences of the letter 'U:' " ++ show (countLetter 'u' str))
    putStrLn ""
    main

countLetter :: Char -> String -> Int
countLetter _ "" = 0
countLetter char [char2]
    | toLower char2 == char = 1
    | otherwise             = 0
countLetter char (x:xs)
    | toLower x == char = 1 + countLetter char xs
    | otherwise         = countLetter char xs

