main = do
    putStrLn "Please enter your string: "
    str <- getLine
    let reverse_string = reverse str
    putStrLn ("You entered " ++ str ++ ", reversed is: " ++ reverse_string)
    putStrLn ""
    main