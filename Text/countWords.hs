-- Count Words in a String – Counts the number of individual words in a string.
-- For added complexity read these strings in from a text file and generate a
-- summary.

main = do
    putStrLn "Enter the file name: "
    fileName <- getLine
    file <- readFile fileName
    putStrLn ("Total number of words: " ++ show (countWords file))
    putStrLn (sayWordNums (words file))
    main

countWords :: String -> Int
countWords x = length (words x)

countCertainWord :: String -> String -> Int
countCertainWord _ "" = 0
countCertainWord s file
    | s == (head fileWords)  = 1 + countCertainWord s tailFile
    | otherwise              = countCertainWord s tailFile 
    where fileWords = words file
          tailFile  = " " `join` (tail fileWords)

join :: String -> [String] -> String
join str xs = concat (map ((++) str) xs)

sayWordNums :: [String] -> String
sayWordNums []  = ""
sayWordNums xs = h ++ " appeared " ++ show n ++ " times.\n" ++
                 sayWordNums (filter (not . (==) h) xs)
                 where h = head xs 
                       n = countCertainWord h (" " `join` xs)
