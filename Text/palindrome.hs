-- A palindrome checker that ignores spaces and case but not punctuation
import Data.Char

main = do 
    putStrLn "Enter string to check: "
    str <- getLine
    let palString = concat (words str)  -- Gets rid of spaces
    putStrLn ("Palindrome check: " ++ show (checkPalindrome palString))
    main


checkPalindrome :: String -> Bool
checkPalindrome s = str == reverse str
                    where str = map toLower s
