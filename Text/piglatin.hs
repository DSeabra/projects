main = do
    putStr "Please enter your word: "
    str <- getLine
    let pig_string = toPigLatin str
    putStrLn ""
    putStrLn ("You entered " ++ str ++ ", pig latin is: " ++ pig_string)
    putStrLn ""
    main
 
isVowel :: Char -> Bool
isVowel c = elem c ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U']
 
toPigLatin :: String -> String  
toPigLatin ar 
    | not (areVowels ar) = ar ++ "ay"
    | otherwise          = toPigLatin' ar False
 
toPigLatin' :: String -> Bool -> String
toPigLatin' [] _  = ""
toPigLatin' [x] _ = [x] ++ "ay"
toPigLatin' (x:xs) False
    | isVowel x = [x] ++ xs ++ "way"
    | otherwise = toPigLatin' (xs ++ [x]) True
toPigLatin' (x:xs) True
    | isVowel x = [x] ++ xs ++ "ay"
    | otherwise = toPigLatin' (xs ++ [x]) True
 
areVowels :: String -> Bool
areVowels []     = False
areVowels [a]    = isVowel a
areVowels (x:xs) = isVowel x || areVowels(xs)
