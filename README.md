This is a fork of Karan's Mega Project List - https://github.com/thekarangoel/Projects

I want to learn Haskell and this is a good way to start, I think. Check out the readmes in each of the folders to see what's in each project. Feel free to take the code and do whatever it is you want to do with it, this is not something I care that much about.

**Text:**

1. Reverse a string
2. Pig latin
3. Number of vowels
4. Check if palindrome
5. Count words in file, give summary of each


**Numbers:**

1. Fibonacci
2. Prime Factorization
3. Next Primes
